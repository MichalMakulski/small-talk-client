# SmallTalk

Terminal client for SmallTalk app.

## Install

```sh
npm install -g git+ssh://git@bitbucket.org:MichalMakulski/small-talk-client.git
```

## Usage

```sh
st <user-name>
```